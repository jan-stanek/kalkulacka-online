<ul id="novinky">
  <li id="novinky_nadpis">Novinky</li>
    <li> 
    <a href="kombinatorika.php">
      <b>1. 12. 2012 </b>
      Přidány výpočty kombinací, variací a permutací
    </a>
  </li>
  <li> 
    <a href="http://www.cesky-internet.cz/obsah/kalkulacka-online-pocitame-jednoduche-priklady-i-narocnejsi-vypocty">
      <b>19. 6. 2012 </b>
      Recenze Kalkulačky na webu Český - Internet
    </a>
  </li>
  <li> 
    <a href="spotreba.php">
      <b>30. 4. 2012 </b>
      Přidán výpočet spotřeby paliva
    </a>
  </li>
  <li> 
    <a href=".">
      <b>26. 11. 2011 </b>
      Základní kalkulačka přidána na úvodní stranu
    </a>
  </li>
  <li> 
    <a href="nejvetsi_spolecny_delitel.php">
      <b>18. 8. 2011 </b>
      Přidán největší společný dělitel
    </a>
  </li>
  <li> 
    <a href="nejmensi_spolecny_nasobek.php">
      <b>18. 8. 2011 </b>
      Přidán nejmenší společný násobek
    </a>
  </li>
  <li> 
    <a href="ciferny_soucet.php">
      <b>29. 6. 2011 </b>
      Ciferný součet přidán
    </a>
  </li>
  <li> 
    <a href="qr_kody.php">
      <b>25. 6. 2011 </b>
      Přidán generátor QR kódů
    </a>
  </li>
  <li> 
    <a href="prvociselny_rozklad.php">
      <b>15. 5. 2011 </b>
      Přidána kalkulačka na prvočíselný rozklad
    </a>
  </li>
  <li> 
    <a href="generator_hesel.php">
      <b>23. 4. 2011 </b>
      Přidán generátor hesel
    </a>
  </li> 
  <li>   
    <a href="prevody_jednotek.php">
      <b>22. 4. 2011 </b>
      Přidány jednotky plochy a objemu v převodech jednotek
    </a>
  </li>
</ul>
<div id="homepage">
<img src="soubory/loga/logo.png" alt="logo" title="logo" align="left" id="logo">
<p>Kalkulačka online shromažďuje na jednom místě mnoho užitečných výpočtů a vzorců rozdělených přehledně do kategorií na: matematické, fyzikální, chemické, finančí a další.</p>
<p>Budeme rádi, když zanecháte Váš názor na web v návštěvní knize.</p>
</div>

<br>

<form name="Kalkulacka" id="kalkulacka" action="">
<table>
  <tr>
    <td colspan="3"><input type="text" name="Pole" Size="20"></td>
    <td><img src="soubory/tlacitka/c.png" alt="c" OnClick="Kalkulacka.Pole.value = ''"></td>
  </tr>
  <tr>
    <td><img src="soubory/tlacitka/7.png" alt="7" OnClick="Kalkulacka.Pole.value += '7'"></td>
    <td><img src="soubory/tlacitka/8.png" alt="8" OnClick="Kalkulacka.Pole.value += '8'"></td>
    <td><img src="soubory/tlacitka/9.png" alt="9" OnClick="Kalkulacka.Pole.value += '9'"></td>
    <td><img src="soubory/tlacitka/deleno.png" alt="÷" OnClick="Kalkulacka.Pole.value += ' / '"></td>
  </tr>
  <tr>
    <td><img src="soubory/tlacitka/4.png" alt="4" OnClick="Kalkulacka.Pole.value += '4'"></td>
    <td><img src="soubory/tlacitka/5.png" alt="5" OnClick="Kalkulacka.Pole.value += '5'"></td>
    <td><img src="soubory/tlacitka/6.png" alt="6" OnClick="Kalkulacka.Pole.value += '6'"></td>
    <td><img src="soubory/tlacitka/krat.png" alt="×" OnClick="Kalkulacka.Pole.value += ' * '"></td>
  </tr>
  <tr>
    <td><img src="soubory/tlacitka/1.png" alt="1" OnClick="Kalkulacka.Pole.value += '1'"></td>
    <td><img src="soubory/tlacitka/2.png" alt="2" OnClick="Kalkulacka.Pole.value += '2'"></td>
    <td><img src="soubory/tlacitka/3.png" alt="3" OnClick="Kalkulacka.Pole.value += '3'"></td>
    <td><img src="soubory/tlacitka/minus.png" alt="-" OnClick="Kalkulacka.Pole.value += ' - '"></td>
  </tr>
  <tr>
    <td><img src="soubory/tlacitka/tecka.png" alt="." OnClick="Kalkulacka.Pole.value += '.'"></td>
    <td><img src="soubory/tlacitka/0.png" alt="0" OnClick="Kalkulacka.Pole.value += '0'"></td>
    <td><img src="soubory/tlacitka/rovno.png" alt="=" OnClick="Kalkulacka.Pole.value = Math.round(eval(Kalkulacka.Pole.value)*10000)/10000"></td>
    <td><img src="soubory/tlacitka/plus.png" alt="+" OnClick="Kalkulacka.Pole.value += ' + '"></td>
  </tr>
</table>
</form>


<div class="clear">&nbsp;</div>



