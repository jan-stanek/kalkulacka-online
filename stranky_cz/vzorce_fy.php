<table bordercolor="#3A6ABE" border="2" class="stred_objekt">
  <tr>
    <th bgcolor="#BAD6FB" width="250">Veličina</td>
    <th bgcolor="#BAD6FB" width="250">Vzorec</th>
  </tr>                   
  <tr>
    <td><b>hustota</b></td>
    <td><img src="stranky_cz/soubory/vzorecky/image21.png"></td>
  </tr>
  <tr>
     <td><b>rychlost</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image22.png"></td>
  </tr>
  <tr>
     <td><b>gravitační síla</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image23.png"></td>
  </tr>
  <tr>
     <td><b>tlak</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image24.png"></td>
  </tr>
  <tr>
     <td><b>hydrostatický tlak</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image25.png"></td>
  </tr>
  <tr>
     <td><b>tlaková síla kapaliny</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image26.png"></td>
  </tr>
  <tr>
     <td><b>vztlaková síla</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image27.png"></td>
  </tr>
  <tr>
     <td><b>práce</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image28.png"></td>
  </tr>
  <tr>
     <td><b>výkon</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image29.png"></td>
  </tr>
  <tr>
     <td><b>potenciální (polohová) energie tělesa</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image30.png"></td>
  </tr>
  <tr>
     <td><b>kinetická (pohybová) energie tělesa</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image31.png"></td>
  </tr>
  <tr>
     <td><b>teplo</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image32.png"></td>
  </tr>
  <tr>
     <td><b>ohmův zákon</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image33.png"></td>
  </tr>
  <tr>
     <td><b>2 rezistory sériově</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image34.png"></td>
  </tr>
  <tr>
     <td><b>2 rezistory paralelně</b></td>
     <td><img src="stranky_cz/soubory/vzorecky/image35.png"></td>
  </tr>
  <tr>
    <td><b>elektrická práce</b></td>
    <td><img src="stranky_cz/soubory/vzorecky/image36.png"></td>
  </tr>
  <tr>
    <td><b>elektrický příkon</b></td>
    <td><img src="stranky_cz/soubory/vzorecky/image37.png"></td>
  </tr>
</table>
