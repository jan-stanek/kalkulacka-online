<center>
<form name="bmi" method="post">
<table border="1">
<tr>
<td align="center">Vaše hmotnost (kg)</td>
<td align="center">Vaše výška (cm)</td>
<td align="center">Vaše BMI</td>
<td align="center">Komentář</td>
</tr>

<tr>
<td><input type="text" name="weight" size="14" onFocus="this.form.weight.value=''"></td>
<td><input type="text" name="height" size="11" onFocus="this.form.height.value=''"></td>
<td><input type="text" name="bmi" size="8" readonly></td>
<td><input type="text" name="my_comment" size="35" readonly></td>
</tr>
</table>
<br>
<input type="button" value="Vypočítej" onClick="computeform(this.form)">
</form>

<SCRIPT LANGUAGE="JAVASCRIPT">
<!-- hide this script tag's contents from old browsers

//Body Mass calculator- by John Scott (johnscott03@yahoo.com)
//Visit JavaScript Kit (http://javascriptkit.com) for script
//Credit must stay intact for use

function ClearForm(form){

    form.weight.value = "";
    form.height.value = "";
    form.bmi.value = "";
    form.my_comment.value = "";

}

function bmi(weight, height) {

          bmindx=weight/eval(height*height);
          return bmindx;
}

function checkform(form) {

       if (form.weight.value==null||form.weight.value.length==0 || form.height.value==null||form.height.value.length==0){
            alert("\nVyplňte nejprve formulář");
            return false;
       }

       else if (parseFloat(form.height.value) <= 0||
                parseFloat(form.height.value) >=500||
                parseFloat(form.weight.value) <= 0||
                parseFloat(form.weight.value) >=500){
                alert("\nReally know what you're doing? \nPlease enter values again. \nWeight in kilos and \nheight in cm");
                ClearForm(form);
                return false;
       }
       return true;

}

function computeform(form) {

       if (checkform(form)) {

       yourbmi=Math.round(bmi(form.weight.value, form.height.value/100));
       form.bmi.value=yourbmi;

       if (yourbmi >40) {
          form.my_comment.value="Obezita 3. stupně (těžká)!";
       }

       else if (yourbmi >35 && yourbmi <=40) {
          form.my_comment.value="Obezita 2. stupně (závažná)";
       }
      
       else if (yourbmi >30 && yourbmi <=35) {
          form.my_comment.value="Obezita 1. stupně";
       }

       else if (yourbmi >25 && yourbmi <=30) {
          form.my_comment.value="Nadváha";
       }

       else if (yourbmi >20 && yourbmi <=25) {
          form.my_comment.value="Normální";
       }

       else if (yourbmi <20) {
          form.my_comment.value="Podváha";
       }

       }
       return;
}
 // -- done hiding from old browsers -->
</SCRIPT></center>
