<table class="menu_tabulka">
  <tr>
    <td><a href="zakladni.php?l=cz"><img src="soubory/ikony/matematika/zakladni.png"><br>Základní kalkulačka</a></td>
    <td><a href="vedecka.php?l=cz"><img src="soubory/ikony/matematika/vedecka.png"><br>Vědecká kalkulačka</a></td>
    <td><a href="odmocnina.php?l=cz"><img src="soubory/ikony/matematika/odmocnina.png"><br>Odmocniny</a></td>
    <td><a href="soustavy.php?l=cz"><img src="soubory/ikony/matematika/soustavy.png"><br>Číselné soustavy</a></td>
  </tr>
  <tr>
    <td><a href="kvadraticka_rovnice.php?l=cz"><img src="soubory/ikony/matematika/kvadraticka_rovnice.png"><br>Kvadratická rovnice</a></td>
    <td><a href="kubicka_rovnice.php?l=cz"><img src="soubory/ikony/matematika/kubicka_rovnice.png"><br>Kubická rovnice</a></td>
    <td><a href="soustava_rovnic.php?l=cz"><img src="soubory/ikony/matematika/soustava_rovnic.png"><br>Soustava rovnic</a></td>
    <td><a href="faktorial.php?l=cz"><img src="soubory/ikony/matematika/faktorial.png"><br>Faktoriál</a></td>
  </tr>
  <tr>
    <td><a href="prvociselny_rozklad.php?l=cz"><img src="soubory/ikony/matematika/prvociselny_rozklad.png"><br>Prvočíselný rozklad</a></td>
    <td><a href="ciferny_soucet.php?l=cz"><img src="soubory/ikony/matematika/ciferny_soucet.png"><br>Ciferný součet</a></td>
    <td><a href="nejvetsi_spolecny_delitel.php?l=cz"><img src="soubory/ikony/matematika/nejvetsi_spolecny_delitel.png"><br>Největší společný dělitel</a></td>
    <td><a href="nejmensi_spolecny_nasobek.php?l=cz"><img src="soubory/ikony/matematika/nejmensi_spolecny_nasobek.png"><br>Nejmenší společný násobek</a></td>
  </tr>
  <tr>
    <td><a href="kombinatorika.php?l=cz"><img src="soubory/ikony/matematika/kombinatorika.png"><br>Kombinatorika</a></td>
    <td><a href="vzorce_m.php?l=cz"><img src="soubory/ikony/matematika/vzorce_m.png"><br>Vzorečky</a></td>
  </tr>      
</table>
