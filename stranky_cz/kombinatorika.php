<script type="text/javascript">
// <![CDATA[
function objGet(o) {
	if (typeof o != 'string') return o;
	else if (document.getElementById) return document.getElementById(o);
	else return null;
	}

window.onload = setType;

function setType() {
	var t = parseInt(objGet('druh').value);
	CalcType = t;
	objGet('k').disabled = (t==5);
	if (t==5) {
		objGet('k').style.display = 'none';
		objGet('klabel').style.display = 'none';
	}
  else {
    objGet('k').style.display = 'inline';
		objGet('klabel').style.display = 'inline';
  }
}

// ]]>
</script>

<center>
<form method="post">
<table id="vstup">
<tr>
  <td colspan="2">
    <select id="druh" name="druh" onchange="setType();">
    	<option value="1">Variace bez opakování</option>
    	<option value="2">Variace s opakováním</option>
    	<option value="3">Kombinace bez opakování</option>
      <option value="4">Kombinace s opakováním</option>
    	<option value="5">Permutace</option>
    </select>
  </td>
</tr>
<tr>
	<td>
		<label for="n" id="nlabel">Počet prvků:</label>
	</td>
	<td>
    <input type="text" name="n" id="n" size="15">
	</td>
</tr>
<tr>
	<td>
		<label for="k" id="klabel">Třída:</label>
	</td>
	<td>
    <input type="text" name="k" id="k" size="5">
	</td>
</tr>
<tr>
  <td colspan="2" align="center">
    <input type="submit" name="ok" value="Vypočítej">
  </td>
</tr>
</table>
</form>
<br>


<?php
$n=$_POST["n"];
$k=$_POST["k"];
$druh=$_POST["druh"];

function factor($cislo){
  $faktorial=1;
  for ($i = 2; $i<=$cislo; $i++){
    $faktorial *= $i;
  }
  return $faktorial;
}

if ($_POST["ok"]=="Vypočítej"){
  switch($druh){
    case 1:
      if ($n<$k){
        echo "chyba";
      }
      elseif ($n != "" and $k != ""){
        echo "<b>V<sub>$k</sub> (".$n.") = ".factor($n)/factor($n-$k)."</b>";
      }
      break;
    case 2:
      if ($n != "" and $k != ""){
        echo "<b>V'<sub>$k</sub> (".$n.") = ".pow($n, $k)."</b>";
      }
      break;
    case 3:
      if ($n<$k){
        echo "chyba";
      }
      elseif ($n != "" and $k != ""){
        echo "<b>C<sub>$k</sub> (".$n.") = ".factor($n)/(factor($n-$k)*factor($k))."</b>";
      }
      break;
    case 4:
      if ($n != "" and $k != ""){
        echo "<b>C'<sub>$k</sub> (".$n.") = ".factor($n+$k-1)/(factor($n-1)*factor($k))."</b>";
      }
      break;
    case 5:
      if ($n != ""){
        echo "<b>P (".$n.") = ".factor($n)."</b>";
      }
      break;
  }
}
?>
</center>



