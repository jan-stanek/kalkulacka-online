<script language="JavaScript">
<!--

// INITIALIZATION:

var blankOption = '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -';

var selectedConversionType = '';

var converterTypes = new Object();

// CONFIGURATION:

converterTypes['Plocha'] = new Array();
converterTypes['Plocha'][0] = 'Akr,4046.86';
converterTypes['Plocha'][1] = 'Ar,100';
converterTypes['Plocha'][2] = 'Hektar,10000';
converterTypes['Plocha'][3] = 'Milimetr čtverečný,0.000001';
converterTypes['Plocha'][4] = 'Centimetr čtverečný,0.0001';
converterTypes['Plocha'][5] = 'Decimetr čtverečný,0.01';
converterTypes['Plocha'][6] = 'Metr čtverečný,1';

converterTypes['Data'] = new Array();
converterTypes['Data'][0] = 'Bit,0.125';
converterTypes['Data'][1] = 'Byte,1';
converterTypes['Data'][2] = 'Gigabyte,1073741824';
converterTypes['Data'][3] = 'Kilobyte,1024';
converterTypes['Data'][4] = 'Megabyte,1048576';

converterTypes['Energie'] = new Array();
converterTypes['Energie'][0] = 'Joule,1';
converterTypes['Energie'][1] = 'Kilojoule,1000';
converterTypes['Energie'][2] = 'Megajoule,1000000';
converterTypes['Energie'][3] = 'Milijoule,0.001';

converterTypes['Délka'] = new Array();
converterTypes['Délka'][0] = 'Centimetr,0.01';
converterTypes['Délka'][1] = 'Stopa,0.3048';
converterTypes['Délka'][2] = 'Palec,0.0254';
converterTypes['Délka'][3] = 'Kilometr,1000';
converterTypes['Délka'][4] = 'Metr,1';
converterTypes['Délka'][5] = 'Míle,1609';
converterTypes['Délka'][6] = 'Milimetr,0.001';
converterTypes['Délka'][7] = 'Námořní míle,1852';
converterTypes['Délka'][8] = 'Yard,0.9144';

converterTypes['Výkon'] = new Array();
converterTypes['Výkon'][0] = 'Gigawatt,1000000000';
converterTypes['Výkon'][1] = 'Koňská síla,746';
converterTypes['Výkon'][2] = 'Kilowatt,1000';
converterTypes['Výkon'][3] = 'Mikrowatt,0.000001';
converterTypes['Výkon'][4] = 'Miliwatt,0.001';
converterTypes['Výkon'][5] = 'Megawatt,1000000';
converterTypes['Výkon'][6] = 'Watt,1';

converterTypes['Rychlost'] = new Array();
converterTypes['Rychlost'][0] = 'Kilometr za hodinu,1';
converterTypes['Rychlost'][1] = 'Kilometr za minutu,60';
converterTypes['Rychlost'][2] = 'Kilometr za sekundu,3600';
converterTypes['Rychlost'][3] = 'Uzel,1.852';
converterTypes['Rychlost'][4] = 'Míle za hodinu,1.609';
converterTypes['Rychlost'][5] = 'Míle za minutu,96.54';
converterTypes['Rychlost'][6] = 'Míle za sekundu,5792.4';

converterTypes['Teplota'] = new Array();
converterTypes['Teplota'][0] = 'Celsius,x';
converterTypes['Teplota'][1] = 'Farenheit,x';
converterTypes['Teplota'][2] = 'Kelvin,x';

converterTypes['Objem'] = new Array();
converterTypes['Objem'][0] = 'Galon,4.54609';
converterTypes['Objem'][1] = 'Litr,1';
converterTypes['Objem'][2] = 'Hektolitr,100';
converterTypes['Objem'][3] = 'Mililitr,0.001';
converterTypes['Objem'][4] = 'Pinta,0.568261';
converterTypes['Objem'][5] = 'Kvarta,1.136522';
converterTypes['Objem'][6] = 'Milimetr krychlový,0.000001';
converterTypes['Objem'][7] = 'Centimetr krychlový,0.001';
converterTypes['Objem'][8] = 'Decimetr krychlový,1';
converterTypes['Objem'][9] = 'Metr krychlový,1000';

converterTypes['Hmotnost'] = new Array();
converterTypes['Hmotnost'][0] = 'Gram,1';
converterTypes['Hmotnost'][1] = 'Kilogram,1000';
converterTypes['Hmotnost'][2] = 'Mikrogram,0.000001';
converterTypes['Hmotnost'][3] = 'Miligram,0.001';
converterTypes['Hmotnost'][4] = 'Libra,453.6';
converterTypes['Hmotnost'][5] = 'Unce,28.35';
converterTypes['Hmotnost'][6] = 'Stone,6350.400000000001';
converterTypes['Hmotnost'][7] = 'Tuna,1000000';

// MAIN:

function changeConversionType(SELECTEDOPTION) {
   if (SELECTEDOPTION.selectedIndex != 0 && SELECTEDOPTION.selectedIndex != 1) {
      var newSelectedConversionType = document.converterForm.convertType.options[SELECTEDOPTION.selectedIndex].text;
      if (newSelectedConversionType != selectedConversionType) {
         selectedConversionType = newSelectedConversionType;
         document.converterForm.convertFrom.options.length = 0;
         document.converterForm.convertTo.options.length = 0;
         for (var convertTypeLoop = 0; convertTypeLoop < converterTypes[newSelectedConversionType].length; convertTypeLoop++) {
            var tempTypeData = converterTypes[newSelectedConversionType][convertTypeLoop].split(',');
            document.converterForm.convertFrom[convertTypeLoop] = new Option(tempTypeData[0],tempTypeData[1]);
            document.converterForm.convertTo[convertTypeLoop] = new Option(tempTypeData[0],tempTypeData[1]);
            }
         document.converterForm.convertFrom[document.converterForm.convertFrom.length] = new Option(blankOption,'');
         document.converterForm.convertTo[document.converterForm.convertTo.length] = new Option(blankOption,'');
         document.converterForm.convertFrom.options[0].selected = true;
         document.converterForm.convertTo.options[0].selected = true;
         document.converterForm.convertOutput.value = document.converterForm.convertInput.value;
         }
      }
   }

function convertInputValue() {
   var tempInputValue = document.converterForm.convertInput.value;
   tempInputValue = tempInputValue.replace(/^\s+|\s+$/g,'');
   if (isNaN(tempInputValue) || tempInputValue == '') {
      alert('Please type in a Number to Convert.');
      document.converterForm.convertInput.focus();
      document.converterForm.convertInput.select();
      }
   else {
      tempInputValue = parseFloat(tempInputValue);
      var tempConvertFrom = document.converterForm.convertFrom.options[document.converterForm.convertFrom.selectedIndex].value;
      var tempConvertTo = document.converterForm.convertTo.options[document.converterForm.convertTo.selectedIndex].value;
      if (tempConvertFrom != '' && tempConvertTo != '') {
         if (selectedConversionType == 'Teplota') {
            var tempConvertFromType = document.converterForm.convertFrom.options[document.converterForm.convertFrom.selectedIndex].text;
            var tempConvertToType = document.converterForm.convertTo.options[document.converterForm.convertTo.selectedIndex].text;
            var tempOutputValue;
            if (tempConvertFromType == 'Celsius' && tempConvertToType == 'Celsius') tempOutputValue = tempInputValue;
            if (tempConvertFromType == 'Celsius' && tempConvertToType == 'Farenheit') tempOutputValue = 9 * tempInputValue / 5 + 32;
            if (tempConvertFromType == 'Celsius' && tempConvertToType == 'Kelvin') tempOutputValue = tempInputValue + 273.15;
            if (tempConvertFromType == 'Farenheit' && tempConvertToType == 'Celsius') tempOutputValue = 5 * (tempInputValue - 32) / 9;
            if (tempConvertFromType == 'Farenheit' && tempConvertToType == 'Farenheit') tempOutputValue = tempInputValue;
            if (tempConvertFromType == 'Farenheit' && tempConvertToType == 'Kelvin') tempOutputValue = 5 * (tempInputValue - 32) / 9 + 273.15;
            if (tempConvertFromType == 'Kelvin' && tempConvertToType == 'Celsius') tempOutputValue = tempInputValue - 273.15;
            if (tempConvertFromType == 'Kelvin' && tempConvertToType == 'Farenheit') tempOutputValue = 9 * (tempInputValue - 273.15) / 5 + 32;
            if (tempConvertFromType == 'Kelvin' && tempConvertToType == 'Kelvin') tempOutputValue = tempInputValue;
            }
         else {
            tempConvertFrom = parseFloat(eval(tempConvertFrom));
            tempConvertTo = parseFloat(eval(tempConvertTo));
            var tempOutputValue = tempInputValue * tempConvertFrom / tempConvertTo;
            }
         document.converterForm.convertOutput.value = tempOutputValue;
         }
      }
   }

//-->
</script>

<center>
<form name="converterForm">

<table>
<tr>
<td align="center">
<select name="convertType" size="1" onChange="javascript:changeConversionType(this);">
<option selected>VYBERTE KATEGORII</option>
<option>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</option>
<option>Data</option>
<option>Délka</option>
<option>Energie</option>
<option>Hmotnost</option>
<option>Objem</option>
<option>Plocha</option>
<option>Rychlost</option>
<option>Teplota</option>
<option>Výkon</option>
</select>
</td>
</tr>
<tr>
<td>
<table cellpadding="5" cellspacing="0" border="0">
<tr>
<td>
<input name="convertInput" type="text" size="15" value="1" onBlur="javascript:convertInputValue();">
<select name="convertFrom" size="1" onChange="javascript:convertInputValue();">
<option>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</option>
</select>
</td>
</tr>
<tr>
<td>
<input name="convertOutput" type="text" size="15" value="1" onBlur="javascript:convertInputValue();">
<select name="convertTo" size="1" onChange="javascript:convertInputValue();">
<option>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</option>
</select>
</td>
</tr>
</table>
</td>
</tr>
</table>
</form>
</center>
