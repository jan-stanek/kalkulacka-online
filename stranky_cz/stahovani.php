<script language="javascript">
<!--
function calc(){
if ((document.ftt.fileunits.value != "")&&(document.ftt.transferunits.value != "")) {
var fileunits = document.ftt.fileunits.value;
var transferspeed = document.ftt.transferspeed.value;   
var transferunits = document.ftt.transferunits.value;  
var filesize1 = document.ftt.filesize.value; 
var seconds = (filesize1*fileunits)/(transferspeed*transferunits);

if (seconds <= 60) 
{document.ftt.outputstuff.value = Math.round(seconds) + " sekund"} 

else {if (seconds <= 3600) {
var minuteswhole1=Math.round(seconds/60);
var leftoversec1 = Math.abs((minuteswhole1-(seconds/60)) * 60);
var leftoversec1 = Math.round(leftoversec1);
document.ftt.outputstuff.value = minuteswhole1 + " minut " + leftoversec1 + " sekund";} 

else {if (seconds <= 86400) {
var hourswhole1 = Math.floor(seconds/3600);
var leftoverhours1 = Math.abs(hourswhole1 - (seconds/3600));
var leftovermindecimal1 = leftoverhours1 * 60;
var wholeleftovermin = Math.floor(leftovermindecimal1);
var leftoversec2 = Math.abs((wholeleftovermin - leftovermindecimal1) * 60);
var leftoversec2 = Math.round(leftoversec2);
document.ftt.outputstuff.value = hourswhole1 + " hodin " + wholeleftovermin + " minut " + leftoversec2 + " sekund";} 
else {
var dayswhole = Math.floor(seconds/86400)
var daysremainingdecimal = Math.abs(dayswhole - (seconds/86400));
var hourswhole2 = Math.floor(daysremainingdecimal * 24);
var leftoverhoursdecimal = Math.abs(hourswhole2 - (daysremainingdecimal * 24));
var leftovermindecimal1 = leftoverhoursdecimal * 60;
var wholeleftovermin2 = Math.floor(leftovermindecimal1);
var leftoversec3 = Math.abs((wholeleftovermin2 - leftovermindecimal1) * 60);
var leftoversec3 = Math.round(leftoversec3);
document.ftt.outputstuff.value = dayswhole + " dní " + hourswhole2 + " hodin " + wholeleftovermin2 + " minut " + leftoversec3 + " sekund";};};};}}
//-->
</script>
<center>

<form name=ftt id=f action=none method=get>

<table>
<tr>
<td>Velikost souboru: </td><td><input name="filesize" type="text" onChange="calc()"></td>
  <td>
     <select name="fileunits" size="1" onChange="calc()" style="width: 188px;">
     <option selected value="">vyberte jednotku velikosti</option> 
     <option value="1">bit</option>
     <option value="8">byte</option> 
     <option value="8192">kilobyte</option> 
     <option value="8388608">megabyte</option>
     <option value="8589934592">gigabyte</option> 
     <option value="8796093022208">terabyte</option> 
     </select>
  </td>
</tr>  
<tr>
<td>Rychlost: </td><td><input name="transferspeed" type="text" onChange="calc()"></td>     
  <td>
     <select name="transferunits" size="1" onChange="calc()" style="width: 188px;">
     <option selected value="">vyberte jednotku rychlosti</option>
     <option value="1">bit/s</option>
     <option value="8">B/s</option>
     <option value="1000">kbit/s</option>
     <option value="8192">kB/s</option>
     <option value="1048576">Mbit/s </option> 
     <option value="8388608">MB/s </option> 
     <option value="1073741824">Gbit/s </option>      
     <option value="8589934592">GB/s </option>
     </select>
  </td>
</tr>     
<tr></tr>
<tr>     
<td>Doba stahování: </td><td colspan="2"><input name="outputstuff" size="52"></td>
</tr>
<tr><td align="center" colspan="3"><input value="Přepočítej" type="button" onClick="calc()"></td></tr>
</table>

</form>
</center>
