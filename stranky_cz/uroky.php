<script type="text/javascript">
// <![CDATA[


function objGet(o) {
	if (typeof o != 'string') return o;
	else if (document.getElementById) return document.getElementById(o);
	else return null;
	}

function round(x,n) {
	var a = Math.pow(10,n);
	return Math.round(x*a)/a;
	}
function lpad(x,n) {
	var a = '000000'+x.toString();
	a = a.substr(a.length-n);
	return a;
	}
function rpad(x,n) {
	var a = round(x,n).toString();
	var f = a.indexOf('.');
	if (f==-1) { a += '.'; f = a.length-1; }
	a += '00000000000000';
	a = a.substr(0,f+n+1);
	return a;
	}
function txtRok(n) {
	if (n==1) return '1&nbsp;rok';
	else if (n==0 || n>4) return n+'&nbsp;let';
	else return n+'&nbsp;roky';
	}
function txtMes(n) {
	if (n==1) return '1&nbsp;měsíc';
	else if (n==0 || n>4) return n+'&nbsp;měsíců';
	else return n+'&nbsp;měsíce';
	}
function Error() {
	var s = new Array();
	for (var i=0;i<arguments.length;i++) s[s.length] = arguments[i];
	alert(s.join(', '));
	}

var Anuit, Urok, Doba, Celkem, CalcType, AnuitType, UrokType, DobaType, N, I, A, K;
var yrCnt, mesCnt, mesUrok, zustUrok, isUver;

function run() {
	Anuit = parseFloat(objGet('anuit').value);
	Urok = parseFloat(objGet('urok').value);
	Doba = parseInt(objGet('doba').value);
	Celkem = parseFloat(objGet('celkem').value);

	isUver = (CalcType>=20);

	AnuitType = (!isUver) ? parseInt(objGet('anuittype1').value) : parseInt(objGet('anuittype2').value);
	UrokType = (!isUver) ? parseInt(objGet('uroktype1').value) : parseInt(objGet('uroktype2').value);
	DobaType = objGet('dobatype').value;
	
	mesCnt = (DobaType=='M') ? Doba : Doba*12;
	yrCnt = (DobaType=='M') ? Math.floor(Doba/12) : Doba;
	mesUrok = (UrokType==2);

	if (!isUver) {
		if (CalcType==13) {		// dobu pocitame vzdy v mesicich
			A = (mesUrok) ? Anuit*12 : Anuit;
			I = (mesUrok) ? Urok/100 : Urok/1200;
			}
		if (!mesUrok) {
			N = yrCnt;
			K = AnuitType;
			A = Anuit*K;
			I = Urok/100;
			}
		else {
			N = mesCnt;
			K = 1;
			A = Anuit;
			I = Urok/1200;
			}
		}
	else {
		A = Anuit;
		K = AnuitType;
		N = Doba;
		I = Urok*UrokType/K/100;
		}

	var Err = '';
	if (CalcType==11) {
		if (!isNaN(Urok) && !isNaN(Doba) && !isNaN(Celkem)) {
			Anuit = round(calcAnuit(),2);
			objGet('anuit').value = rpad(Anuit,2);
			objGet('resnadpis').innerHTML = '<h3>Vypočítaný vklad: ' + rpad(Anuit,2) + '&nbsp;Kč<'+'/h3>';
			}
		else Err = 'Musíte zadat úrok, dobu spoření a cílovou částku';
		}
	else if (CalcType==12) {
		if (!isNaN(Anuit) && !isNaN(Doba) && !isNaN(Celkem)) {
			Urok = round(calcUrok(),2);
			objGet('urok').value = rpad(Urok,2);
			objGet('resnadpis').innerHTML = '<h3>Vypočítaný úrok: ' + rpad(Urok,2) + ' % p. a.<'+'/h3>';
			}
		else Err = 'Musíte zadat vklad, dobu spoření a cílovou částku';
		}
	else if (CalcType==13) {
		if (!isNaN(Anuit) && !isNaN(Urok) && !isNaN(Celkem)) {
			Doba = calcDoba();
			mesCnt = Doba;
			objGet('doba').value = Doba;
			objGet('dobatype').selectedIndex = 0;
			var dd = txtMes(Doba);
			if (Doba>11) dd += ' (' + txtRok(Math.floor(Doba/12)) + ', ' + txtMes(Doba%12) + ')';
			objGet('resnadpis').innerHTML = '<h3>Vypočítaná doba spoření: ' + dd + '<'+'/h3>';
			}
		else Err = 'Musíte zadat vklad, úrok a cílovou částku';
		}
	else if (CalcType==14) {
		if (!isNaN(Anuit) && !isNaN(Urok) && !isNaN(Doba)) {
			Celkem = round(calcCelkem(),2);
			objGet('celkem').value = rpad(Celkem,2);
			objGet('resnadpis').innerHTML = '<h3>Vypočítaná cílová částka: ' + rpad(Celkem,2) + '&nbsp;Kč<'+'/h3>';
			}
		else Err = 'Musíte zadat vklad, úrok a dobu spoření';
		}
	
	else if (CalcType==21) {
		if (!isNaN(Urok) && !isNaN(Doba) && !isNaN(Celkem)) {
			Anuit = round(calcUAnuit(),2);
			objGet('anuit').value = rpad(Anuit,2);
			var dd = '<p>Celkové navýšení o '+rpad(Anuit*N-Celkem,2)+'&nbsp;Kč ('+rpad((Anuit*N/Celkem-1)*100,2)+'&nbsp;%)<'+'/p>';
			objGet('resnadpis').innerHTML = '<h3>Vypočítaná splátka: ' + rpad(Anuit,2) + '&nbsp;Kč<'+'/h3>'+dd;
			}
		else Err = 'Musíte zadat úrok, počet splátek a výši úvěru';
		}
	else if (CalcType==22) {
		if (!isNaN(Anuit) && !isNaN(Doba) && !isNaN(Celkem)) {
			Urok = round(calcUUrok(),2);
			objGet('urok').value = rpad(Urok,2);
//			objGet('uroktype2').selectedIndex = 0;
			var dd = '<p>Celkové navýšení o '+rpad(Anuit*N-Celkem,2)+'&nbsp;Kč ('+rpad((Anuit*N/Celkem-1)*100,2)+'&nbsp;%)<'+'/p>';
			objGet('resnadpis').innerHTML = '<h3>Vypočítaný úrok: ' + rpad(Urok,2) + ' %<'+'/h3>'+dd;
			}
		else Err = 'Musíte zadat splátku, počet splátek a výši úvěru';
		}
	else if (CalcType==23) {
		if (!isNaN(Anuit) && !isNaN(Urok) && !isNaN(Celkem)) {
			Doba = calcUDoba();
			N = Doba;
			var cc = round(calcUCelkem(),2);
			if (cc<Celkem) { N++; Doba++ }
			objGet('doba').value = Doba;
			var dd = '<p>Celkové navýšení o '+rpad(Anuit*N-Celkem,2)+'&nbsp;Kč ('+rpad((Anuit*N/Celkem-1)*100,2)+'&nbsp;%)<'+'/p>';
			objGet('resnadpis').innerHTML = '<h3>Vypočítaný počet splátek: ' + Doba + '<'+'/h3>'+dd;
			}
		else Err = 'Musíte zadat splátku, úrok a výši úvěru';
		}
	else if (CalcType==24) {
		if (!isNaN(Anuit) && !isNaN(Urok) && !isNaN(Doba)) {
			Celkem = round(calcUCelkem(),2);
			objGet('celkem').value = rpad(Celkem,2);
			var dd = '<p>Celkové navýšení o '+rpad(Anuit*N-Celkem,2)+'&nbsp;Kč ('+rpad((Anuit*N/Celkem-1)*100,2)+'&nbsp;%)<'+'/p>';
			objGet('resnadpis').innerHTML = '<h3>Vypočítaná výše úvěru: ' + rpad(Celkem,2) + '&nbsp;Kč<'+'/h3>'+dd;
			}
		else Err = 'Musíte zadat výšku splátky, úrok a počet splátek';
		}
	
	if (!Err) Draw(0);
	else Error(Err);
	

	}


function calcCelkem() {
	var jn;
	if (AnuitType==0) jn = Anuit * Math.pow(1+I,N);
	else {
		jn = (I) ? A * ( (1+I)*(Math.pow(1+I,N)-1)/I ) : A * N;
		if (!mesUrok) jn += (mesCnt-12*N)*Anuit;
		}
	return jn;
	}
function calcUCelkem() {
	var j0;
	if (I) j0 = A / ( I*Math.pow(1+I,N) / (Math.pow(1+I,N)-1) );
	else j0 = A * N;
	return j0;
	}
function calcAnuit() {
	var a;
	if (!isUver && AnuitType==0) a = Celkem / Math.pow(1+I,N);
	else {
		if (I) a = Celkem / ( (1+I)*(Math.pow(1+I,N)-1)/I ) / K;
		else a = Celkem / N / K;
		}
	return a;
	}
function calcUAnuit() {
	var a;
	if (I) a = Celkem * ( I*Math.pow(1+I,N) / (Math.pow(1+I,N)-1) );
	else a = Celkem / N;
	return a;
	}
function calcDoba() {
	var n;
	if (AnuitType==0) {
		if (I==0) {
			n = 0;
			if (Celkem!=Anuit) Error('Dobu nelze spočítat');
			}
		else {
			n = Math.ceil( round( Math.log(Celkem/Anuit)/Math.log(1+I) ,4) );
			if (!mesUrok) n = n*12;
			}
		}
	else {
		if (I==0) {
			n = Math.ceil(Celkem/Anuit);
			}
		else {
			n = Math.ceil( round( Math.log(I*Celkem/(A*(1+I))+1)/Math.log(1+I) ,4) );
			if (!mesUrok) {		// rocni uroceni - mozna muzeme skoncit driv
				if (n>0) {
					var c = round ( A * ( (1+I)*(Math.pow(1+I,n)-1)/I ), 2);
					if (c>Celkem) {
						c = Celkem - A * ( (1+I)*(Math.pow(1+I,n-1)-1)/I );;
						n = (n-1)*12 + Math.ceil(c/Anuit);
						}
					else n = n*12;
					}
				else n = 0;
				}
			}
		}
	return n;
	}
function calcUDoba() {
	if (I==0) return Math.ceil(Celkem/A);
	else {
		var n=0,c=0;
		while (c<Celkem) {
			n++;
			c = A / ( I*Math.pow(1+I,n) / (Math.pow(1+I,n)-1) );
			}
		return n;
		}
	}
function calcUrok() {
	var it=0, maxIt=100, c1,cm, Im, I1=0, I2, search=true, found=0;
	var vlozeno = (AnuitType==0) ? Anuit : mesCnt*Anuit;
	if (N>0) I2 = (mesUrok) ? (Celkem/vlozeno-1)/12 : Celkem/vlozeno-1;
	else search = false;
	while (search && it<maxIt) {
		it++;
		Im = (I1+I2)/2;
		if (Math.abs(I2-I1)<=0.00005) {
			search = false;
			found = (mesUrok) ? Im*1200 : Im*100;
			}
		else {
			I = I1; c1 = calcCelkem(); I = Im; cm = calcCelkem();
			if (c1<Celkem && cm>Celkem) I2 = Im; else I1 = Im;
			}
		}
	if (search) { Error('Úrok nelze spočítat'); return 0; }
	else return found;
	}
function calcUUrok() {
	var it=0, maxIt=100, c1,cm, Im, I1=0, I2, search=true, found=0;
	var vlozeno = N*A;
	if (N>0) I2 = vlozeno/Celkem-1;
	else search = false;
	while (search && it<maxIt) {
		it++;
		Im = (I1+I2)/2;
		if (Math.abs(I2-I1)<=0.00005) {
			search = false;
			found = Im*K/UrokType*100;
			}
		else {
			I = I1; c1 = calcUCelkem(); I = Im; cm = calcUCelkem();
			if (c1>Celkem && cm<Celkem) I2 = Im; else I1 = Im;
			}
		}
	if (search) { Error('Úrok nelze spočítat'); return 0; }
	else return found;
	}

function Draw(on) {
	if (on) {
		var i,j,cl,r,m, u,v, sumu=0, sumv=0, sum=0;
		var buff = '<table border="1" cellpadding="5" cellspacing="0" style="margin:auto;"><tr class="hdr">';
		if (isUver) {
			buff += '<th>Č. splátky (rok/měsíc)<'+'/th>';
			buff += '<th>Splátka<'+'/th>';
			buff += '<th>Úrok<'+'/th>';
			buff += '<th>Dlužná částka<'+'/th><'+'/tr>';
			buff += '<tr><td colspan="3">&nbsp;<'+'/td><td class="kc">'+rpad(Celkem,2)+' Kč<'+'/td><'+'/tr>';
			sum = Celkem;
			for (i=0;i<Doba;i++) {
				r = Math.floor(i/K)+1;
				m = (i*12/K)%12 + 1;
				cl = ((i+1)%10==0) ? ' class="rok"' : '';
				v = Anuit;
				u = sum*I;
				sumv += v;
				sumu += u;
				sum -= v - u;
				if (sum<0) {
					sum += v-u; sumv -= v; sumu -= u;
					v = sum; u = 0; sumv += v;
					sum = 0;
					}
				buff += '<tr'+cl+'>';
				buff += '<td class="mes"><b>'+(i+1)+'.<'+'/b> ('+lpad(r,2)+'/'+lpad(m,2)+')<'+'/td>';
				buff += '<td class="kc">'+rpad(v,2)+'&nbsp;Kč<'+'/td>';
				buff += '<td class="kc">'+rpad(u,2)+'&nbsp;Kč<'+'/td>';
				buff += '<td class="kc"><b>'+rpad(sum,2)+'&nbsp;Kč<'+'/b><'+'/td>';
				buff += '<'+'/tr>';
				}
			buff += '<tr class="sum"><th>Celkem:<'+'/th>';
			buff += '<td class="kc">'+rpad(round(sumv,1),2)+'&nbsp;Kč<'+'/td>';
			buff += '<td class="kc">'+rpad(round(sumu,1),2)+'&nbsp;Kč<'+'/td>';
			buff += '<td class="kc">'+rpad(round(sum,1),2)+'&nbsp;Kč<'+'/td>';
			}
		else {
			buff += '<th>Měsíc (rok/měsíc)<'+'/th>';
			buff += '<th>Vklad<'+'/th>';
			buff += '<th>Úrok<'+'/th>';
			buff += '<th>Stav účtu<'+'/th><'+'/tr>';
			for (i=0;i<mesCnt;i++) {
				r = Math.floor(i/12)+1;
				m = i%12 + 1;
				cl = ((i+1)%12==0 || i==mesCnt-1) ? ' class="rok"' : '';
				v = (AnuitType==0 && i>0) ? 0 : Anuit;
				sumv += v;
				u = (mesUrok || (i+1)%12==0 ) ? (sum+v)*I : 0;
				sumu += u;
				sum += v + u;
				buff += '<tr'+cl+'>';
				buff += '<td class="mes"><b>'+(i+1)+'.<'+'/b> ('+lpad(r,2)+'/'+lpad(m,2)+')<'+'/td>';
				buff += '<td class="kc">'+rpad(v,2)+'&nbsp;Kč<'+'/td>';
				buff += '<td class="kc">'+rpad(u,2)+'&nbsp;Kč<'+'/td>';
				buff += '<td class="kc"><b>'+rpad(sum,2)+'&nbsp;Kč<'+'/b><'+'/td>';
				buff += '<'+'/tr>';
				}
			buff += '<tr class="sum"><th>Celkem:<'+'/th>';
			buff += '<td class="kc">'+rpad(round(sumv,1),2)+'&nbsp;Kč<'+'/td>';
			buff += '<td class="kc">'+rpad(round(sumu,1),2)+'&nbsp;Kč<'+'/td>';
			buff += '<td class="kc">'+rpad(round(sum,1),2)+'&nbsp;Kč<'+'/td>';
			}
		buff += '<'+'/tr><'+'/table>';
		buff += '<p><button onclick="Draw(0)"> Skrýt tabulku <'+'/button><'+'/p>';
		objGet('results').innerHTML = buff;
		}
	else
		objGet('results').innerHTML = '<p><button onclick="Draw(1)"> Zobrazit tabulku <'+'/button><'+'/p>';
	}

window.onload = setType;

function setType() {
	var t = parseInt(objGet('calctype').value);
	CalcType = t;
	objGet('anuit').disabled = (t==11 || t==21);
	objGet('urok').disabled = (t==12 || t==22);
	objGet('doba').disabled = (t==13 || t==23);
	objGet('celkem').disabled = (t==14 || t==24);
	if (t<20) {
		objGet('anuitlabel').innerHTML = 'Vklad:';
		objGet('anuittype1').style.display = 'inline';
		objGet('anuittype2').style.display = 'none';
		objGet('uroktype1').style.display = 'inline';
		objGet('uroktype2').style.display = 'none';
		objGet('dobalabel').innerHTML = 'Období:';
		objGet('dobatype').style.display = 'inline';
		objGet('celkemlabel').innerHTML = 'Cílová částka:';
		}
	else {
		objGet('anuitlabel').innerHTML = 'Výše splátky:';
		objGet('anuittype1').style.display = 'none';
		objGet('anuittype2').style.display = 'inline';
		objGet('uroktype1').style.display = 'none';
		objGet('uroktype2').style.display = 'inline';
		objGet('dobalabel').innerHTML = 'Počet splátek:';
		objGet('dobatype').style.display = 'none';
		objGet('celkemlabel').innerHTML = 'Výše úvěru:';
		}
	objGet('resnadpis').innerHTML = '';
	objGet('results').innerHTML = '';
	}

function setExample(a,at,u,ut,d,dt,c,t) {
	objGet('anuittype1').style.display = 'inline';
	objGet('anuittype2').style.display = 'inline';
	objGet('uroktype1').style.display = 'inline';
	objGet('uroktype2').style.display = 'inline';
	objGet('dobatype').style.display = 'inline';
	objGet('calctype').selectedIndex = t;
	objGet('anuit').value = a;
	if (t>3) objGet('anuittype2').selectedIndex = at; else objGet('anuittype1').selectedIndex = at;
	objGet('urok').value = u;
	if (t>3) objGet('uroktype2').selectedIndex = ut; else objGet('uroktype1').selectedIndex = ut;
	objGet('doba').value = d;
	objGet('dobatype').selectedIndex = dt;
	objGet('celkem').value = c;
	setType(); run();
	}

// ]]>
</script>

<center>
<form>
<table id="settings">
<tr>
  <td></td>
  <td>
    <select id="calctype" onchange="setType();">
  	<option value="11">Spoření: spočítat vklad</option>
  	<option value="12">Spoření: spočítat úrok</option>
  	<option value="13">Spoření: spočítat dobu spoření</option>
  
  	<option value="14" selected="selected">Spoření: spočítat cílovou částku</option>
  	<option value="21">Úvěr: spočítat splátku</option>
  	<option value="22">Úvěr: spočítat úrok</option>
  	<option value="23">Úvěr: spočítat počet splátek</option>
  	<option value="24">Úvěr: spočítat výši úvěru</option>
    </select>
  </td>
</tr>
<tr>
	<td>
		<label for="anuit" id="anuitlabel">Vklad:</label>
	</td>
	<td><input type="text" name="anuit" id="anuit" size="15" value="" />&nbsp;Kč
		<select id="anuittype1">
			<option value="0">jednorázový</option>
			<option value="12" selected="selected">měsíčně</option>

		</select>
		<select id="anuittype2" style="display:none;">
			<option value="12" selected="selected">měsíčně</option>
			<option value="4">čtvrtletně</option>
			<option value="2">pololetně</option>
			<option value="1">ročně</option>
		</select>

	</td>
</tr>
<tr>
	<td>
		<label for="urok">Úrok:</label>
	</td>
	<td><input type="text" name="urok" id="urok" size="5" value="" /> %
		<select id="uroktype1">
			<option value="1">p. a., připisovaný na konci roku</option>

			<option value="2" selected="selected">p. a., připisovaný měsíčně</option>
		</select>
		<select id="uroktype2" style="display:none;">
			<option value="1" selected="selected">ročně (p. a.)</option>
			<option value="2">pololetně</option>
			<option value="4">čtvrtletně</option>
			<option value="12">měsíčně (p. m.)</option>

		</select>
	</td>
</tr>
<tr>
	<td>
		<label for="doba" id="dobalabel">Období:</label>
	</td>
	<td><input type="text" name="doba" id="doba" size="5" value="" />
		<select id="dobatype">

			<option value="M">měsíců</option>
			<option value="Y" selected="selected">let</option>
		</select>
	</td>
</tr>
<tr>
	<td>
		<label for="celkem" id="celkemlabel">Cílová částka:</label>

	</td>
	<td><input type="text" name="celkem" id="celkem" size="15" value="" />&nbsp;Kč</td>
</tr>
<tr>
  <td></td>
  <td>
    <input type="hidden" name="page" value="uroky">
    <input type="button" id="button" onclick="run();return false" value="Vypočítat">
  </td>
</tr>
</table>

</form>
</center>





