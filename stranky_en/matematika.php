<table class="menu_tabulka">
  <tr>
    <td><a href="zakladni.php?l=en"><img src="soubory/ikony/matematika/zakladni.png"><br>Basic calculator</a></td>
    <td><a href="vedecka.php?l=en"><img src="soubory/ikony/matematika/vedecka.png"><br>Scientific calculator</a></td>
    <td><a href="odmocnina.php?l=en"><img src="soubory/ikony/matematika/odmocnina.png"><br>Roots</a></td>
    <td><a href="soustavy.php?l=en"><img src="soubory/ikony/matematika/soustavy.png"><br>Numeral systems</a></td>
  </tr>
  <tr>
    <td><a href="kvadraticka_rovnice.php?l=en"><img src="soubory/ikony/matematika/kvadraticka_rovnice.png"><br>Quadratic equation</a></td>
    <td><a href="kubicka_rovnice.php?l=en"><img src="soubory/ikony/matematika/kubicka_rovnice.png"><br>Cubic equation</a></td>
    <td><a href="soustava_rovnic.php?l=en"><img src="soubory/ikony/matematika/soustava_rovnic.png"><br>System of equations</a></td>
    <td><a href="faktorial.php?l=en"><img src="soubory/ikony/matematika/faktorial.png"><br>Factorial</a></td>
  </tr>
  <tr>
    <td><a href="prvociselny_rozklad.php?l=en"><img src="soubory/ikony/matematika/prvociselny_rozklad.png"><br>Prime factorization</a></td>
    <td><a href="ciferny_soucet.php?l=en"><img src="soubory/ikony/matematika/ciferny_soucet.png"><br>Digit sum</a></td>
    <td><a href="nejvetsi_spolecny_delitel.php?l=en"><img src="soubory/ikony/matematika/nejvetsi_spolecny_delitel.png"><br>Greatest common divisor</a></td>
    <td><a href="nejmensi_spolecny_nasobek.php?l=en"><img src="soubory/ikony/matematika/nejmensi_spolecny_nasobek.png"><br>Least common multiple</a></td>
  </tr>      
  <tr>
    <td><a href="kombinatorika.php?l=en"><img src="soubory/ikony/matematika/kombinatorika.png"><br>Combinatorics</a></td>
    <td><a href="vzorce_m.php?l=en"><img src="soubory/ikony/matematika/vzorce_m.png"><br>Formulas</a></td>
  </tr>
</table>
