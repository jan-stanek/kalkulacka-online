<ul id="novinky">
  <li id="novinky_nadpis">News</li>
  <li> 
    <a href=".">
      <b>26. 11. 2011 </b>
      Basic calculator has been added on homepage
    </a>
  </li>
  <li> 
    <a href="http://www.kalkulacka-online.com">
      <b>25. 11. 2011 </b>
      Domain has been purchased
    </a>
  </li>
  <li> 
    <a href="nejvetsi_spolecny_delitel.php">
      <b>18. 8. 2011 </b>
      Greatest common divisor has been added
    </a>
  </li>
  <li> 
    <a href="nejmensi_spolecny_nasobek.php">
      <b>18. 8. 2011 </b>
      Least common multiple has been added
    </a>
  </li>
  <li> 
    <a href="ciferny_soucet.php">
      <b>29. 6. 2011 </b>
      Digit sum has been added
    </a>
  </li>
  <li> 
    <a href="qr_kody.php">
      <b>25. 6. 2011 </b>
      QR code generator has been added
    </a>
  </li>
  <li> 
    <a href="prvociselny_rozklad.php">
      <b>15. 5. 2011 </b>
      Prime factorization has been added
    </a>
  </li>
  <li> 
    <a href="generator_hesel.php">
      <b>23. 4. 2011 </b>
      Password generator has been added
    </a>
  </li> 
  <li>   
    <a href="prevody_jednotek.php">
      <b>22. 4. 2011 </b>
      Units of area and volume has been added
    </a>
  </li>
</ul>
<div id="homepage">
  <img src="soubory/loga/logo.png" alt="logo" title="logo" align="left" id="logo">
  <p>Online calculator collects many useful calculations and formulas in one place divided into categories of mathematical, physical, chemical, finance and more.</p>
  <p>We'll be happy if you leave your opinion in the guestbook.</p> 
</div>

<br>

<form name="Kalkulacka" id="kalkulacka" action="">
<table>
  <tr>
    <td colspan="3"><input type="text" name="Pole" Size="20"></td>
    <td><img src="soubory/tlacitka/c.png" alt="c" OnClick="Kalkulacka.Pole.value = ''"></td>
  </tr>
  <tr>
    <td><img src="soubory/tlacitka/7.png" alt="7" OnClick="Kalkulacka.Pole.value += '7'"></td>
    <td><img src="soubory/tlacitka/8.png" alt="8" OnClick="Kalkulacka.Pole.value += '8'"></td>
    <td><img src="soubory/tlacitka/9.png" alt="9" OnClick="Kalkulacka.Pole.value += '9'"></td>
    <td><img src="soubory/tlacitka/deleno.png" alt="÷" OnClick="Kalkulacka.Pole.value += ' / '"></td>
  </tr>
  <tr>
    <td><img src="soubory/tlacitka/4.png" alt="4" OnClick="Kalkulacka.Pole.value += '4'"></td>
    <td><img src="soubory/tlacitka/5.png" alt="5" OnClick="Kalkulacka.Pole.value += '5'"></td>
    <td><img src="soubory/tlacitka/6.png" alt="6" OnClick="Kalkulacka.Pole.value += '6'"></td>
    <td><img src="soubory/tlacitka/krat.png" alt="×" OnClick="Kalkulacka.Pole.value += ' * '"></td>
  </tr>
  <tr>
    <td><img src="soubory/tlacitka/1.png" alt="1" OnClick="Kalkulacka.Pole.value += '1'"></td>
    <td><img src="soubory/tlacitka/2.png" alt="2" OnClick="Kalkulacka.Pole.value += '2'"></td>
    <td><img src="soubory/tlacitka/3.png" alt="3" OnClick="Kalkulacka.Pole.value += '3'"></td>
    <td><img src="soubory/tlacitka/minus.png" alt="-" OnClick="Kalkulacka.Pole.value += ' - '"></td>
  </tr>
  <tr>
    <td><img src="soubory/tlacitka/tecka.png" alt="." OnClick="Kalkulacka.Pole.value += '.'"></td>
    <td><img src="soubory/tlacitka/0.png" alt="0" OnClick="Kalkulacka.Pole.value += '0'"></td>
    <td><img src="soubory/tlacitka/rovno.png" alt="=" OnClick="Kalkulacka.Pole.value = Math.round(eval(Kalkulacka.Pole.value)*10000)/10000"></td>
    <td><img src="soubory/tlacitka/plus.png" alt="+" OnClick="Kalkulacka.Pole.value += ' + '"></td>
  </tr>
</table>
</form>

<div class="clear">&nbsp;</div>
