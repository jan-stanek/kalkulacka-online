<script language="JavaScript">
<!--

// INITIALIZATION:

var blankOption = '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -';

var selectedConversionType = '';

var converterTypes = new Object();

// CONFIGURATION:

converterTypes['Area'] = new Array();
converterTypes['Area'][0] = 'Acre,4046.86';
converterTypes['Area'][1] = 'Are,100';
converterTypes['Area'][2] = 'Hectare,10000';
converterTypes['Area'][3] = 'Square millimeter,0.000001';
converterTypes['Area'][4] = 'Square centimeter,0.0001';
converterTypes['Area'][5] = 'Square decimeter,0.01';
converterTypes['Area'][6] = 'Square meter,1';


converterTypes['Data'] = new Array();
converterTypes['Data'][0] = 'Bits,0.125';
converterTypes['Data'][1] = 'Bytes,1';
converterTypes['Data'][2] = 'Gigabytes,1073741824';
converterTypes['Data'][3] = 'Kilobytes,1024';
converterTypes['Data'][4] = 'Megabytes,1048576';

converterTypes['Energy'] = new Array();
converterTypes['Energy'][0] = 'Joules,1';
converterTypes['Energy'][1] = 'Kilojoules,1000';
converterTypes['Energy'][2] = 'Megajoules,1000000';
converterTypes['Energy'][3] = 'Millijoules,0.001';

converterTypes['Length'] = new Array();
converterTypes['Length'][0] = 'Centimetres,0.01';
converterTypes['Length'][1] = 'Feet,0.3048';
converterTypes['Length'][2] = 'Inches,0.0254';
converterTypes['Length'][3] = 'Kilometres,1000';
converterTypes['Length'][4] = 'Metres,1';
converterTypes['Length'][5] = 'Miles,1609';
converterTypes['Length'][6] = 'Millimetres,0.001';
converterTypes['Length'][7] = 'Nautical Miles,1852';
converterTypes['Length'][8] = 'Yards,0.9144';

converterTypes['Power'] = new Array();
converterTypes['Power'][0] = 'Gigawatts,1000000000';
converterTypes['Power'][1] = 'Horsepower,746';
converterTypes['Power'][2] = 'Kilowatts,1000';
converterTypes['Power'][3] = 'Microwatts,0.000001';
converterTypes['Power'][4] = 'Milliwatts,0.001';
converterTypes['Power'][5] = 'Megawatts,1000000';
converterTypes['Power'][6] = 'Watts,1';

converterTypes['Speed'] = new Array();
converterTypes['Speed'][0] = 'Kilometres per Hour,1';
converterTypes['Speed'][1] = 'Kilometres per Minute,60';
converterTypes['Speed'][2] = 'Kilometres per Second,3600';
converterTypes['Speed'][3] = 'Knots,1.852';
converterTypes['Speed'][4] = 'Miles per Hour,1.609';
converterTypes['Speed'][5] = 'Miles per Minute,96.54';
converterTypes['Speed'][6] = 'Miles per Second,5792.4';

converterTypes['Temperature'] = new Array();
converterTypes['Temperature'][0] = 'Celsius,x';
converterTypes['Temperature'][1] = 'Farenheit,x';
converterTypes['Temperature'][2] = 'Kelvin,x';

converterTypes['Volume'] = new Array();
converterTypes['Volume'][0] = 'Gallons,4.54609';
converterTypes['Volume'][1] = 'Litres,1';
converterTypes['Volume'][2] = 'Hectolitres,100';
converterTypes['Volume'][3] = 'Millilitres,0.001';
converterTypes['Volume'][4] = 'Pints,0.568261';
converterTypes['Volume'][5] = 'Quarts,1.136522';
converterTypes['Volume'][6] = 'Cubic millimeter,0.000001';
converterTypes['Volume'][7] = 'Cubic centimeter,0.001';
converterTypes['Volume'][8] = 'Cubic decimeter,1';
converterTypes['Volume'][9] = 'Cubic meter,1000';

converterTypes['Weight'] = new Array();
converterTypes['Weight'][0] = 'Grams,1';
converterTypes['Weight'][1] = 'Kilograms,1000';
converterTypes['Weight'][2] = 'Micrograms,0.000001';
converterTypes['Weight'][3] = 'Milligrams,0.001';
converterTypes['Weight'][4] = 'Pounds,453.6';
converterTypes['Weight'][5] = 'Ounces,28.35';
converterTypes['Weight'][6] = 'Stones,6350.400000000001';
converterTypes['Weight'][7] = 'Tonnes,1000000';

// MAIN:

function changeConversionType(SELECTEDOPTION) {
   if (SELECTEDOPTION.selectedIndex != 0 && SELECTEDOPTION.selectedIndex != 1) {
      var newSelectedConversionType = document.converterForm.convertType.options[SELECTEDOPTION.selectedIndex].text;
      if (newSelectedConversionType != selectedConversionType) {
         selectedConversionType = newSelectedConversionType;
         document.converterForm.convertFrom.options.length = 0;
         document.converterForm.convertTo.options.length = 0;
         for (var convertTypeLoop = 0; convertTypeLoop < converterTypes[newSelectedConversionType].length; convertTypeLoop++) {
            var tempTypeData = converterTypes[newSelectedConversionType][convertTypeLoop].split(',');
            document.converterForm.convertFrom[convertTypeLoop] = new Option(tempTypeData[0],tempTypeData[1]);
            document.converterForm.convertTo[convertTypeLoop] = new Option(tempTypeData[0],tempTypeData[1]);
            }
         document.converterForm.convertFrom[document.converterForm.convertFrom.length] = new Option(blankOption,'');
         document.converterForm.convertTo[document.converterForm.convertTo.length] = new Option(blankOption,'');
         document.converterForm.convertFrom.options[0].selected = true;
         document.converterForm.convertTo.options[0].selected = true;
         document.converterForm.convertOutput.value = document.converterForm.convertInput.value;
         }
      }
   }

function convertInputValue() {
   var tempInputValue = document.converterForm.convertInput.value;
   tempInputValue = tempInputValue.replace(/^\s+|\s+$/g,'');
   if (isNaN(tempInputValue) || tempInputValue == '') {
      alert('Please type in a Number to Convert.');
      document.converterForm.convertInput.focus();
      document.converterForm.convertInput.select();
      }
   else {
      tempInputValue = parseFloat(tempInputValue);
      var tempConvertFrom = document.converterForm.convertFrom.options[document.converterForm.convertFrom.selectedIndex].value;
      var tempConvertTo = document.converterForm.convertTo.options[document.converterForm.convertTo.selectedIndex].value;
      if (tempConvertFrom != '' && tempConvertTo != '') {
         if (selectedConversionType == 'Temperature') {
            var tempConvertFromType = document.converterForm.convertFrom.options[document.converterForm.convertFrom.selectedIndex].text;
            var tempConvertToType = document.converterForm.convertTo.options[document.converterForm.convertTo.selectedIndex].text;
            var tempOutputValue;
            if (tempConvertFromType == 'Celsius' && tempConvertToType == 'Celsius') tempOutputValue = tempInputValue;
            if (tempConvertFromType == 'Celsius' && tempConvertToType == 'Farenheit') tempOutputValue = 9 * tempInputValue / 5 + 32;
            if (tempConvertFromType == 'Celsius' && tempConvertToType == 'Kelvin') tempOutputValue = tempInputValue + 273.15;
            if (tempConvertFromType == 'Farenheit' && tempConvertToType == 'Celsius') tempOutputValue = 5 * (tempInputValue - 32) / 9;
            if (tempConvertFromType == 'Farenheit' && tempConvertToType == 'Farenheit') tempOutputValue = tempInputValue;
            if (tempConvertFromType == 'Farenheit' && tempConvertToType == 'Kelvin') tempOutputValue = 5 * (tempInputValue - 32) / 9 + 273.15;
            if (tempConvertFromType == 'Kelvin' && tempConvertToType == 'Celsius') tempOutputValue = tempInputValue - 273.15;
            if (tempConvertFromType == 'Kelvin' && tempConvertToType == 'Farenheit') tempOutputValue = 9 * (tempInputValue - 273.15) / 5 + 32;
            if (tempConvertFromType == 'Kelvin' && tempConvertToType == 'Kelvin') tempOutputValue = tempInputValue;
            }
         else {
            tempConvertFrom = parseFloat(eval(tempConvertFrom));
            tempConvertTo = parseFloat(eval(tempConvertTo));
            var tempOutputValue = tempInputValue * tempConvertFrom / tempConvertTo;
            }
         document.converterForm.convertOutput.value = tempOutputValue;
         }
      }
   }

//-->
</script>

<center>
<form name="converterForm">

<table>
<tr>
<td align="center">
<select name="convertType" size="1" onChange="javascript:changeConversionType(this);">
<option selected>SELECT A CONVERSION CATEGORY</option>
<option>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</option>
<option>Area</option>
<option>Data</option>
<option>Energy</option>
<option>Length</option>
<option>Power</option>
<option>Speed</option>
<option>Temperature</option>
<option>Weight</option>
<option>Volume</option>
</select>
</td>
</tr>
<tr>
<td>
<table cellpadding="5" cellspacing="0" border="0">
<tr>
<td>
<input name="convertInput" type="text" size="15" value="1" onBlur="javascript:convertInputValue();">
<select name="convertFrom" size="1" onChange="javascript:convertInputValue();">
<option>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</option>
</select>
</td>
</tr>
<tr>
<td>
<input name="convertOutput" type="text" size="15" value="1" onBlur="javascript:convertInputValue();">
<select name="convertTo" size="1" onChange="javascript:convertInputValue();">
<option>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</option>
</select>
</td>
</tr>
</table>
</td>
</tr>
</table>
</form>
</center>
