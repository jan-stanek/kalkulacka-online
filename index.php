<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<?php

/*
$file = fopen("ip.txt","a");
fwrite($file,$_SERVER['REMOTE_ADDR']."\n");
fclose($file);
*/

if ($_GET["l"] != "cz" and $_GET["l"] != "en" and !isset($_COOKIE["jazyk"])){
  $user_l = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2);
  if ($user_l == "cs" or $user_l == "sk"){$l = "cz";}
  elseif ($user_l == "en"){$l = "en";}
  else {$l = "cz";}
}
elseif ($_GET["l"] == "cz" or $_GET["l"] == "en"){
  $l = $_GET["l"];
  setcookie ("jazyk", $l, time()+3600*24*365);
}
elseif ($_COOKIE["jazyk"] == "cz" or $_COOKIE["jazyk"] == "en"){
  $l = $_COOKIE["jazyk"];
  setcookie ("jazyk", $l, time()+3600*24*365);
}

$p = $_GET["p"];
if ($p == "index"){
  $p = "homepage";
}

if ($l == "cz"){
  switch ($p){
    case "homepage":$titulek = "Úvod"; break;
    case "matematika":$titulek = "Matematika"; break;
    case "fyzika":$titulek = "Fyzika"; break;
    case "chemie":$titulek = "Chemie"; break;
    case "penize":$titulek = "Peníze"; break;    
    case "ostatni":$titulek = "Ostatní"; break;
    case "o_webu":$titulek = "O webu"; break;
    
    case "zakladni":$titulek = "Základní kalkulačka"; break;
    case "vedecka":$titulek = "Vědecká kalkulačka"; break;
    case "odmocnina":$titulek = "Odmocniny"; break;
    case "soustavy":$titulek = "Číselné soustavy"; break;
    case "kvadraticka_rovnice":$titulek = "Kvadratická rovnice"; break;
    case "kubicka_rovnice":$titulek = "Kubická rovnice"; break;
    case "soustava_rovnic":$titulek = "Soustava rovnic"; break;
    case "faktorial":$titulek = "Faktoriál"; break;
    case "prvociselny_rozklad":$titulek = "Prvočíselný rozklad"; break;
    case "ciferny_soucet":$titulek = "Ciferný součet"; break;
    case "nejvetsi_spolecny_delitel":$titulek = "Největší společný dělitel"; break;
    case "nejmensi_spolecny_nasobek":$titulek = "Nejmenší společný násobek"; break;
    case "kombinatorika":$titulek = "Kombinatorika"; break;
    case "vzorce_m":$titulek = "Vzorečky"; break;
    
    case "rezistory":$titulek = "Hodnoty rezistorů"; break;
    case "vzorce_fy":$titulek = "Vzorečky"; break;
    
    case "vzorce_ch":$titulek = "Vzorečky"; break;
    
    case "uroky":$titulek = "Spoření, splátky"; break;
    
    case "nahodne_cislo":$titulek = "Náhodné číslo"; break;
    case "prevody_jednotek":$titulek = "Převody jednotek"; break;
    case "stahovani":$titulek = "Doba stahování"; break;
    case "chmod":$titulek = "Přístupová práva (chmod)"; break;
    case "rgb":$titulek = "RGB barvy"; break;
    case "bmi":$titulek = "BMI"; break;
    case "generator_hesel":$titulek = "Generátor hesel"; break;    
    case "qr_kody":$titulek = "Generátor QR kódů"; break;
    case "spotreba":$titulek = "Spotřeba paliva"; break;
    
    case "navstevni_kniha":$titulek = "Návštěvní kniha"; break;
    case "kontakt":$titulek = "Kontakt"; break; 
  }
  if ($p == "homepage"){
    $nazev = "Kalkulačka online";
    $popis = "Web plný užitečných matematických, fyzikálních, chemických a finančních výpočtů a vzorečků.";
    $key = "kalkulačka, online, výpočet, peníze, vědecká kalkulačka, matematika, fyzika";
  }
  else {
    $nazev = "Kalkulačka online - $titulek";
    $popis = "$titulek - Web plný užitečných matematických, fyzikálních, chemických a finančních výpočtů a vzorečků.";
    $key = "kalkulačka, online, výpočet, peníze, vědecká kalkulačka, matematika, fyzika";
  }
  $nadpis = "Kalkulačka online";  
}
elseif ($l == "en"){
  switch ($p){
    case "homepage":$titulek = "Home"; break;
    case "matematika":$titulek = "Math"; break;
    case "fyzika":$titulek = "Physics"; break;
    case "chemie":$titulek = "Chemistry"; break;
    case "penize":$titulek = "Money"; break;    
    case "ostatni":$titulek = "Other"; break;
    case "o_webu":$titulek = "About"; break;
    
    case "zakladni":$titulek = "Basic calculator"; break;
    case "vedecka":$titulek = "Scientific calculator"; break;
    case "odmocnina":$titulek = "Roots"; break;
    case "soustavy":$titulek = "Numeral systems"; break;
    case "kvadraticka_rovnice":$titulek = "Quadratic equation"; break;
    case "kubicka_rovnice":$titulek = "Cubic equation"; break;
    case "soustava_rovnic":$titulek = "System of equations"; break;
    case "faktorial":$titulek = "Factorial"; break;
    case "prvociselny_rozklad":$titulek = "Prime factorization"; break;
    case "ciferny_soucet":$titulek = "Digit sum"; break;
    case "nejvetsi_spolecny_delitel":$titulek = "Greatest common divisor"; break;
    case "nejmensi_spolecny_nasobek":$titulek = "Least common multiple"; break;
    case "kombinatorika":$titulek = "Combinatorics"; break;
    case "vzorce_m":$titulek = "Formulas"; break;
    
    case "rezistory":$titulek = "Resistors values"; break;
    case "vzorce_fy":$titulek = "Formulas"; break;
    
    case "vzorce_ch":$titulek = "Formulas"; break;
    
    case "uroky":$titulek = "Savings, installment"; break;
    
    case "nahodne_cislo":$titulek = "Random number"; break;
    case "prevody_jednotek":$titulek = "Unit conversion"; break;
    case "stahovani":$titulek = "Download time"; break;
    case "chmod":$titulek = "Access rights (chmod)"; break;
    case "rgb":$titulek = "RGB colors"; break;
    case "bmi":$titulek = "BMI"; break;
    case "generator_hesel":$titulek = "Password generator"; break;
    case "qr_kody":$titulek = "QR code generator"; break;   
    case "spotreba":$titulek = "Fuel consumption"; break;
    
    case "navstevni_kniha":$titulek = "Guestbook"; break;
    case "kontakt":$titulek = "Contact"; break; 
  }
  if ($p == "homepage"){
    $nazev = "Online calculator";
    $popis = "Site full of useful mathematical, physical, chemical and financial calculations and formulas.";
    $key = "calculator, online, calculation, money, scientific calculator, math, physics";
  }
  else {
    $nazev = "Online calculator - $titulek";
    $popis = "$titulek - Site full of useful mathematical, physical, chemical and financial calculations and formulas.";
    $key = "calculator, online, calculation, money, scientific calculator, math, physics";
  }
  $nadpis = "Online calculator";
}

?>
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="description" content="<?php echo $popis; ?>">
<meta name="keywords" content="<?php echo $key; ?>"> 
<meta name="author" content="Jan Staněk"> 
<meta name="robots" content="index, follow">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="styl/style.css" rel="stylesheet" type="text/css" media="screen">
<link rel="shortcut icon" href="styl/favicon.ico">
<title><?php echo $nazev; ?></title>

<!--[if lt IE 7.]>
<script defer type="text/javascript" src="script/pngfix.js"></script>
<![endif]--> 

</head>

<body>
<div id="stranka">
	<div id="hlavicka">
		<?php
      echo "<h1>".$nadpis."</h1>";	
      echo "<img id='nadpis' src='styl/logo_$l.png' alt='$nadpis'>";
    ?>
  </div>
                                                  
  <div id="jazyk">
    <a href="<?php echo $p;?>.php?l=cz"><img id="vlajka_cz" src="styl/cz.png" alt="cz"></a>
    <a href="<?php echo $p;?>.php?l=en"><img id="vlajka_en" src="styl/en.png" alt="en"></a>
  </div>

<!--  
  <div id="donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
      <input type="hidden" name="cmd" value="_s-xclick">
      <input type="hidden" name="hosted_button_id" value="CQALWLTRP7CZQ">
      <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" name="submit" alt="PayPal - The safer, easier way to pay online!">
      <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
    </form>
  </div>
-->  
  <div id="menu">
  <?php
  if ($l == "cz")
  {
    echo "
  		<ul>
  			<li "; if ($p == "homepage"){echo "class='vybrana'";} echo "><a href='.' class='menu_a'>Úvod</a>
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "matematika" or $p == "zakladni" or $p == "vedecka" or $p == "odmocnina" or $p == "soustavy" or $p == "kvadraticka_rovnice" or $p == "kubicka_rovnice" or $p == "soustava_rovnic" or $p == "vzorce_m" or $p == "faktorial" or $p == "prvociselny_rozklad" or $p == "ciferny_soucet" or $p == "nejvetsi_spolecny_delitel" or $p == "nejmensi_spolecny_nasobek"){echo "class='vybrana'";} echo "><a href='matematika.php?l=$l' class='menu_a'>Matematika</a>
          <ul>
            <li><a href='zakladni.php?l=$l'>Základní kalkulačka</a></li>
            <li><a href='vedecka.php?l=$l'>Vědecká kalkulačka</a></li>
            <li><a href='odmocnina.php?l=$l'>Odmocniny</a></li>
            <li><a href='soustavy.php?l=$l'>Číselné soustavy</a></li>
            <li><a href='kvadraticka_rovnice.php?l=$l'>Kvadratická rovnice</a></li>
            <li><a href='kubicka_rovnice.php?l=$l'>Kubická rovnice</a></li>
            <li><a href='soustava_rovnic.php?l=$l'>Soustava rovnic</a></li>
            <li><a href='faktorial.php?l=$l'>Faktoriál</a></li>            
            <li><a href='prvociselny_rozklad.php?l=$l'>Prvočíselný rozklad</a></li>
            <li><a href='ciferny_soucet.php?l=$l'>Ciferný součet</a></li>             
            <li><a href='nejvetsi_spolecny_delitel.php?l=$l'>Největší společný dělitel</a></li>             
            <li><a href='nejmensi_spolecny_nasobek.php?l=$l'>Nejmenší společný násobek</a></li>             
            <li><a href='kombinatorika.php?l=$l'>Kombinatorika</a></li>
            <li><a href='vzorce_m.php?l=$l'>Vzorečky</a></li>              
          </ul>
        </li>
      </ul>
  		<ul>
      	<li "; if ($p == "fyzika" or $p == "rezistory" or $p == "vzorce_fy"){echo "class='vybrana'";} echo "><a href='fyzika.php?l=$l' class='menu_a'>Fyzika</a>
          <ul>
            <li><a href='rezistory.php?l=$l'>Hodnoty rezistorů</a></li>
            <li><a href='vzorce_fy.php?l=$l'>Vzorečky</a></li>      	   
          </ul>
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "chemie" or $p == "vzorce_ch"){echo "class='vybrana'";} echo "><a href='chemie.php?l=$l' class='menu_a'>Chemie</a>
          <ul>
            <li><a href='vzorce_ch.php?l=$l'>Vzorečky</a></li>
          </ul>  
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "penize" or $p == "uroky"){echo "class='vybrana'";} echo "><a href='penize.php?l=$l' class='menu_a'>Peníze</a>
          <ul>
            <li><a href='uroky.php?l=$l'>Spoření, splátky</a></li>
          </ul>   			
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "ostatni" or $p == "rgb" or $p == "prevody_jednotek" or $p == "bmi" or $p == "chmod" or $p == "stahovani" or $p == "nahodne_cislo" or $p == "generator_hesel" or $p == "qr_kody"){echo "class='vybrana'";} echo "><a href='ostatni.php?l=$l' class='menu_a'>Ostatní</a>
          <ul>
            <li><a href='nahodne_cislo.php?l=$l'>Náhodné číslo</a></li>
            <li><a href='prevody_jednotek.php?l=$l'>Převody jednotek</a></li>
            <li><a href='stahovani.php?l=$l'>Doba stahování</a></li>
            <li><a href='chmod.php?l=$l'>Přístupová práva (chmod)</a></li>
            <li><a href='rgb.php?l=$l'>RGB barvy</a></li>
            <li><a href='bmi.php?l=$l'>BMI</a></li>            
            <li><a href='generator_hesel.php?l=$l'>Generátor hesel</a></li>     
            <li><a href='qr_kody.php?l=$l'>Generátor QR kódů</a></li>
            <li><a href='spotreba.php?l=$l'>Spotřeba paliva</a></li>    
          </ul>   			
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "o_webu" or $p == "navstevni_kniha" or $p == "kontakt"){echo "class='vybrana'";} echo "><a href='o_webu.php?l=$l' class='menu_a'>O webu</a>
          <ul>
            <li><a href='navstevni_kniha.php?l=$l'>Návštěvní kniha</a></li>
            <li><a href='kontakt.php?l=$l'>Kontakt</a></li>  
          </ul>
        </li>
  		</ul>
  		";
  }
  elseif ($l == "en")
  {
    echo "
      <ul>
  			<li "; if ($p == "homepage"){echo "class='vybrana'";} echo "><a href='.' class='menu_a'>Home</a>
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "matematika" or $p == "zakladni" or $p == "vedecka" or $p == "odmocnina" or $p == "soustavy" or $p == "kvadraticka_rovnice" or $p == "kubicka_rovnice" or $p == "soustava_rovnic" or $p == "vzorce_m" or $p == "faktorial" or $p == "prvociselny_rozklad" or $p == "ciferny_soucet" or $p == "nejvetsi_spolecny_delitel" or $p == "nejmensi_spolecny_nasobek"){echo "class='vybrana'";} echo "><a href='matematika.php?l=$l' class='menu_a'>Math</a>
          <ul>
            <li><a href='zakladni.php?l=$l'>Basic calculator</a></li>
            <li><a href='vedecka.php?l=$l'>Scientific calculator</a></li>
            <li><a href='odmocnina.php?l=$l'>Roots</a></li>
            <li><a href='soustavy.php?l=$l'>Numeral systems</a></li>
            <li><a href='kvadraticka_rovnice.php?l=$l'>Quadratic equation</a></li>
            <li><a href='kubicka_rovnice.php?l=$l'>Cubic equation</a></li>
            <li><a href='soustava_rovnic.php?l=$l'>System of equations</a></li>
            <li><a href='faktorial.php?l=$l'>Factorial</a></li>            
            <li><a href='prvociselny_rozklad.php?l=$l'>Prime factorization</a></li>
            <li><a href='ciferny_soucet.php?l=$l'>Digit sum</a></li>
            <li><a href='nejvetsi_spolecny_delitel.php?l=$l'>Greatest common divisor</a></li>             
            <li><a href='nejmensi_spolecny_nasobek.php?l=$l'>Least common multiple</a></li>
            <li><a href='kombinatorika.php?l=$l'>Combinatorics</a></li>  
            <li><a href='vzorce_m.php?l=$l'>Formulas</a></li>              
          </ul>
        </li>
      </ul>
  		<ul>
      	<li "; if ($p == "fyzika" or $p == "rezistory" or $p == "vzorce_fy"){echo "class='vybrana'";} echo "><a href='fyzika.php?l=$l' class='menu_a'>Physics</a>
          <ul>
            <li><a href='rezistory.php?l=$l'>Resistors values</a></li>
            <li><a href='vzorce_fy.php?l=$l'>Formulas</a></li>      	   
          </ul>
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "chemie" or $p == "vzorce_ch"){echo "class='vybrana'";} echo "><a href='chemie.php?l=$l' class='menu_a'>Chemistry</a>
          <ul>
            <li><a href='vzorce_ch.php?l=$l'>Formulas</a></li>
          </ul>  
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "penize" or $p == "uroky"){echo "class='vybrana'";} echo "><a href='penize.php?l=$l' class='menu_a'>Money</a>
          <ul>
            <li><a href='uroky.php?l=$l'>Savings, installment</a></li>
          </ul>   			
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "ostatni" or $p == "rgb" or $p == "prevody_jednotek" or $p == "bmi" or $p == "chmod" or $p == "stahovani" or $p == "nahodne_cislo" or $p == "generator_hesel" or $p == "qr_kody"){echo "class='vybrana'";} echo "><a href='ostatni.php?l=$l' class='menu_a'>Other</a>
          <ul>
            <li><a href='nahodne_cislo.php?l=$l'>Random number</a></li>
            <li><a href='prevody_jednotek.php?l=$l'>Unit conversion</a></li>
            <li><a href='stahovani.php?l=$l'>Download time</a></li>
            <li><a href='chmod.php?l=$l'>Access rights (chmod)</a></li>
            <li><a href='rgb.php?l=$l'>RGB colors</a></li>
            <li><a href='bmi.php?l=$l'>BMI</a></li>
            <li><a href='generator_hesel.php?l=$l'>Password generator</a></li>
            <li><a href='qr_kody.php?l=$l'>QR code generator</a></li>    
            <li><a href='spotreba.php?l=$l'>Fuel consumption</a></li>                         
          </ul>   			
        </li>
  		</ul>
  		<ul>
  			<li "; if ($p == "o_webu" or $p == "navstevni_kniha" or $p == "kontakt"){echo "class='vybrana'";} echo "><a href='o_webu.php?l=$l' class='menu_a'>About</a>
          <ul>
            <li><a href='navstevni_kniha.php?l=$l'>Guestbook</a></li>
            <li><a href='kontakt.php?l=$l'>Contact</a></li>  
          </ul>
        </li>
  		</ul>
      ";
  }  
  ?>		
	</div>  
  <div id="obsah">
	   <!--[if IE]>
    <style type="text/css">
      #obsah {_height: 500px;} 
    </style>
    <![endif]-->
    
    <?php
      include "./stranky_".$l."/".$p.".php";
    ?>
	</div>     

    <?php
    echo "<div id='sdilet'>";  
      if ($l=="cz"){
        echo '
        <div class="addthis_toolbox addthis_default_style">
        <a href="http://www.addthis.com/bookmark.php?v=250&amp;username=jansta" class="addthis_button_compact">Sdílet</a>
        <span class="addthis_separator">|</span>
        <a class="addthis_button_facebook"></a>
        <a class="addthis_button_twitter"></a>
        <a class="addthis_button_google_plusone"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=jansta"></script>';
      }
      elseif ($l=="en"){
        echo '        
        <div class="addthis_toolbox addthis_default_style">
        <a href="http://www.addthis.com/bookmark.php?v=250&amp;username=jansta" class="addthis_button_compact">Share</a>
        <span class="addthis_separator">|</span>
        <a class="addthis_button_facebook"></a>
        <a class="addthis_button_twitter"></a>
        <a class="addthis_button_google_plusone"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=jansta"></script>';
      }
    echo "</div>";      

    echo '<div id="socialni_site">';
      if ($l=="cz"){
        echo '
        <a href="http://www.facebook.com/kalkulacka.online" target="blank"><img src="soubory/loga/facebook_cz.png" title="Přidejte si nás na Facebooku!" alt="Přidejte si nás na Facebooku!"></a>
        <a href="https://plus.google.com/107240008760231075639" target="blank"><img src="soubory/loga/googleplus_cz.png" title="Přidejte si nás na Google+!" alt="Přidejte si nás na Google+!"></a>
        ';
      }
      elseif ($l=="en"){
        echo '
        <a href="http://www.facebook.com/kalkulacka.online" target="blank"><img src="soubory/loga/facebook_en.png" title="Join us on Facebook!" alt="Join us on Facebook!"></a>
        <a href="https://plus.google.com/107240008760231075639" target="blank"><img src="soubory/loga/googleplus_en.png" title="Join us on Google+!" alt="Join us on Google+!"></a>
        ';
      }
    echo "</div>";    

    echo "<center><div class='doporucujeme_blok'>";
      if ($l == "cz"){echo "<span class='doporucujeme_nadpis'>Doporučujeme:  </span>";}
      elseif ($l == "en"){echo "<span class='doporucujeme_nadpis'>Recommend:  </span>";}
      
      if ($l=="cz")
        echo "<a href=\"https://play.google.com/store/apps/details?id=cz.jstanek.transferit\" title=\"TransferIT!\">TransferIT!</a> - jednoduchý přenos souborů pro Android a PC.";
      else
        echo "<a href=\"https://play.google.com/store/apps/details?id=cz.jstanek.transferit\" title=\"TransferIT!\">TransferIT!</a> - easy file transfer for Android and PC.";
     
      echo " • ";
      
      echo "<a href=\"http://www.hypotecniarena.cz/hypotecni-kalkulacka\" title=\"hypoteční kalkulačka\">hypoteční kalkulačka</a> přesný výpočet hypoték";
    echo "</div></center>";

    ?>
  
  <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-9757753-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  </script>
</div>

<div id="paticka">
	<p>&copy; Kalkulačka online 2008-<?php echo date("Y");?></p>
</div>

<!--
        <iframe src="http://www.cpmleader.com/b_160x600.php?id=8848" width="160" height="600" frameborder="no" marginheight="0" marginwidth="0" scrolling="no" id="banner_l"></iframe>
        <iframe src="http://www.cpmleader.com/b_160x600.php?id=8848" width="160" height="600" frameborder="no" marginheight="0" marginwidth="0" scrolling="no" id="banner_r"></iframe>
-->
</body>
</html>
